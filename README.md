# book_shelf

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Demo in Gitlab pages
See [Demo of this project](https://damorz.gitlab.io/book_shelf/).
